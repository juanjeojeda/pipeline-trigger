"""Tests for triggers.brew_trigger."""
from datetime import datetime
import queue
import unittest
from unittest import mock

from freezegun import freeze_time
import proton.reactor
import yaml

from tests import fakes
import triggers.brew_trigger as brew


def _prepare(trigger):
    """Prepare a trigger similar to poll_triggers."""
    config = {
        '.amqp': {
            'server_url': 'server_url',
            'web_url': 'web_url',
            'top_url': 'top_url',
        }
    }
    trigger['.extends'] = '.amqp'
    config[trigger.get('name', 'name')] = trigger
    return brew.get_triggers_from_config(config)[0]


class TestPollTriggers(unittest.TestCase):
    """Tests for brew.poll_triggers()."""

    @mock.patch('triggers.brew_trigger.ThreadPoolExecutor.submit')
    @mock.patch('triggers.brew_trigger.as_completed')
    @mock.patch('triggers.brew_trigger.AMQP091Receiver.receive_messages')
    @mock.patch('triggers.brew_trigger.AMQP091Receiver.__init__')
    @mock.patch('triggers.brew_trigger.AMQPReceiver.__init__')
    @mock.patch('proton.reactor.Container.run')
    def test_trigger_init(self, mock_run, mock_amqp_init, mock_amqp091_init,
                          mock_receive, mock_completed, mock_submit):
        """Verify trigger initialization.

        Verify the configured triggers are cleaned up to only contain strings
        so GitLab accepts them and the container is started.
        """
        config_text = '{}'.format(
            '.amqp:\n'
            '  .cert_path: filepath\n'
            '  .message_topics:\n'
            '    - topic.task.closed\n'
            '  .receiver_urls:\n'
            '    - url:port\n'
            '    - url2:port\n'
            '  server_url: server_url\n'
            '  web_url: web_url\n'
            '  top_url: top_url\n'
            '.amqp091:\n'
            '  .routing_keys:\n'
            '    - org.fedoraproject.prod.buildsys.build.state.change\n'
            '    - org.fedoraproject.prod.buildsys.task.state.change\n'
            '    - org.fedoraproject.prod.copr.build.end\n'
            '  .host: rabbitmq.fedoraproject.org\n'
            '  .port: 5671\n'
            '  .cafile: /etc/cki/umb/fedora-ca.pem\n'
            '  .certfile: /etc/cki/umb/fedora-cert.pem\n'
            '  .virtual_host: /public_pubsub\n'
            '  .exchange: amq.topic\n'
            '.default:\n'
            '  mail_from: name@email.org\n'
            'project_name:\n'
            '  .extends: .amqp\n'
            '  cki_project: username/project\n'
            '  rpm_release: fc28\n'
            '  package_name: kernel\n'
            '  send_report_to_upstream: True\n'
            '  send_report_on_success: True\n'
            '  cki_pipeline_branch: test_name\n'
            'project_name2:\n'
            '  .extends: .amqp\n'
            '  cki_project: username/project\n'
            '  rpm_release: fc29\n'
            '  package_name: kernel\n'
            '  cki_pipeline_branch: test_name2\n'
        )
        config = yaml.safe_load(config_text)

        mock_amqp_init.return_value = None
        mock_amqp091_init.return_value = None
        mock_receive.return_value = None
        mock_submit.return_value = None
        mock_completed.return_value = []

        brew.poll_triggers('gitlab_instance', config)
        called_with_amqp = mock_amqp_init.call_args
        self.assertEqual(called_with_amqp[0][0], 'filepath')
        self.assertEqual(called_with_amqp[0][1], ['url:port', 'url2:port'])
        self.assertEqual(called_with_amqp[0][2], ['topic.task.closed'])
        self.assertEqual(called_with_amqp[0][3], '.amqp')
        self.assertIsInstance(called_with_amqp[0][4], queue.Queue)

        called_with_amqp091 = mock_amqp091_init.call_args
        mock_amqp091_init.assert_called_with(config['.amqp091'], '.amqp091',
                                             mock.ANY)
        self.assertIsInstance(called_with_amqp091[0][2], queue.Queue)


class TestOnStart(unittest.TestCase):
    """Tests for brew_trigger.AMQPReceiver.on_start()."""

    @mock.patch('proton._reactor.Container.create_receiver')
    @mock.patch('proton._transport.SSLDomain.set_credentials')
    @mock.patch('proton._reactor.Container.connect')
    def test_topics(self, mock_connect, mock_credentials, mock_create):
        """Verify receiver is set up for all passed topics."""
        mock_connect.return_value = 'connection'
        test_topics = ['test_topic1', 'test_topic2']
        event = fakes.Event()
        event.container = proton.reactor.Container()

        receiver = brew.AMQPReceiver('cert_path',
                                     ['url1', 'url2'],
                                     test_topics,
                                     None,
                                     None)
        receiver.on_start(event)

        mock_connect.assert_called()
        mock_credentials.assert_called()
        self.assertEqual(len(test_topics), mock_create.call_count)


class TestSanityCheck(unittest.TestCase):
    """Tests for brew_trigger.sanity_check()."""

    def test_bad_message(self):
        """Verify sanity check returns False if the new state is not CLOSED."""
        properties = {'new': 'still open'}
        self.assertFalse(brew.sanity_check(properties))

    def test_bad_method(self):
        """Verify sanity check returns False if this is not a build message.

        Check for build method. If that one is not present, we require a
        build_id attribute.
        """
        properties = {'new': 'CLOSED', 'attribute': 'state', 'method': 'nope'}
        self.assertFalse(brew.sanity_check(properties))

        # No method nor build_id
        properties = {'new': 'CLOSED', 'attribute': 'state'}
        self.assertFalse(brew.sanity_check(properties))

    def test_bad_new(self):
        """Verify sanity check returns False if the build is not successful.

        Check for the value of 'new' attribute. For koji, we require 1; for
        brew 'CLOSED' is required.
        """
        properties = {'new': 'FAILED', 'attribute': 'state', 'method': 'build'}
        self.assertFalse(brew.sanity_check(properties))

        properties = {'new': 2, 'attribute': 'state', 'build_id': 111}
        self.assertFalse(brew.sanity_check(properties))

    def test_good_message(self):
        """Verify sanity check returns True if the message is good."""
        properties = {'new': 'CLOSED', 'attribute': 'state', 'method': 'build'}
        self.assertTrue(brew.sanity_check(properties))

        properties = {'new': 'CLOSED', 'attribute': 'state', 'build_id': 111}
        self.assertTrue(brew.sanity_check(properties))

        properties = {'topic': '/topic/VirtualTopic.cki.results', 'msg_id': 11}
        self.assertTrue(brew.sanity_check(properties))


class TestProcessMessage(unittest.TestCase):
    """Tests for brew_trigger.process_message()."""

    @mock.patch('triggers.brew_trigger.put_gitlab_queue')
    def test_skip_result_pipe(self, mock_trigger):
        """Check we don't process result message triggers."""
        trigger = _prepare({'result_pipe': 'True'})
        message = {'status': 1,
                   'owner': 'owner',
                   'copr': 'repo1',
                   'pkg': 'kernel',
                   'version': '5.0-11.fc30'}

        brew.process_copr('gitlab', [trigger], message)
        self.assertFalse(mock_trigger.called)


class TestProcessCOPR(unittest.TestCase):
    """Tests for brew_trigger.process_copr()."""

    @freeze_time("2019-01-01")
    @mock.patch('triggers.brew_trigger.put_gitlab_queue')
    def test_pipeline_triggered(self, mock_trigger):
        """Check we trigger a pipeline for a valid COPR build."""
        message = {'status': 1,
                   'user': 'user',
                   'copr': 'repo1',
                   'pkg': 'kernel',
                   'version': '5.0-11.fc30',
                   'build': 123,
                   'chroot': 'fedora-30-x86_64',
                   'owner': 'pkgowner'}
        trigger = _prepare({'rpm_release': 'fc30',
                            'package_name': 'kernel',
                            '.coprs': ['pkgowner/repo1', 'pkgowner/repo2']})

        brew.process_copr('gitlab', [trigger], message)

        expected_trigger = {
            'rpm_release': 'fc30',
            'package_name': 'kernel',
            'nvr': '{}-{}'.format(message['pkg'], message['version']),
            'name': 'name',
            'title': 'COPR: {}-{}: x86_64'.format(message['pkg'],
                                                  message['version']),
            'owner': 'user',
            'arch_override': 'x86_64',
            'repo_name': 'pkgowner/repo1',
            'cki_pipeline_type': 'copr',
            'copr_build': '123',
            'discovery_time': f'{datetime.utcnow().isoformat()}Z',
            'server_url': 'server_url',
            'web_url': 'web_url',
            'top_url': 'top_url'
        }
        mock_trigger.assert_called_with('gitlab', expected_trigger)

    def test_bad_status(self):
        """Verify we don't run the pipeline for unsuccessful builds."""
        message = {'status': 2,
                   'pkg': 'kernel',
                   'version': '5.0-11.fc30'}
        nvr = '{}-{}'.format(message['pkg'], message['version'])

        with self.assertLogs(level='DEBUG', logger=brew.LOGGER) as log:
            brew.process_copr('gitlab', [], message)
            self.assertIn(f'COPR build for {nvr} not successful',
                          log.output[-1])

    @mock.patch('triggers.brew_trigger.put_gitlab_queue')
    def test_no_pkg_version(self, mock_trigger):
        """Verify we return right after detecting pkg or version is missing."""
        message = {}
        brew.process_copr('gitlab', [], message)
        self.assertFalse(mock_trigger.called)

        # Still missing version
        message = {'pkg': 'something'}
        brew.process_copr('gitlab', [], message)
        self.assertFalse(mock_trigger.called)

    def test_no_pipeline(self):
        """Check we don't trigger a pipeline for non-configured builds."""
        message = {'status': 1,
                   'owner': 'different-owner',
                   'copr': 'repo1',
                   'pkg': 'kernel',
                   'version': '5.0-11.fc30'}
        nvr = '{}-{}'.format(message['pkg'], message['version'])
        trigger = _prepare({'rpm_release': 'fc30',
                            'package_name': 'kernel',
                            '.coprs': ['owner/repo1', 'owner/repo2']})

        with self.assertLogs(level='DEBUG', logger=brew.LOGGER) as log:
            brew.process_copr('gitlab', [trigger], message)
            self.assertIn(f'COPR: Pipeline for {nvr} not configured',
                          log.output[-1])

    @mock.patch('triggers.brew_trigger.put_gitlab_queue')
    def test_skip_result_pipe(self, mock_trigger):
        """Check we don't process result message triggers."""
        trigger = _prepare({'result_pipe': 'True'})
        message = {'status': 1,
                   'owner': 'owner',
                   'copr': 'repo1',
                   'pkg': 'kernel',
                   'version': '5.0-11.fc30'}

        brew.process_copr('gitlab', [trigger], message)
        self.assertFalse(mock_trigger.called)

    def test_bad_package_name(self):
        """Check we don't continue if the package name doesn't match."""
        message = {'status': 1,
                   'owner': 'owner',
                   'copr': 'repo1',
                   'pkg': 'python-metakernel',
                   'version': '0.21-11.fc30'}
        nvr = '{}-{}'.format(message['pkg'], message['version'])
        trigger = _prepare({'rpm_release': 'fc30',
                            'package_name': 'kernel',
                            '.coprs': ['owner/repo1', 'owner/repo2']})

        with self.assertLogs(level='DEBUG', logger=brew.LOGGER) as log:
            brew.process_copr('gitlab', [trigger], message)
            self.assertIn(f'COPR: Pipeline for {nvr} not configured',
                          log.output[-1])


class TestProcessResultMessage(unittest.TestCase):
    """Test cases for brew_trigger.process_result_message()."""

    def test_no_trigger(self):
        """Check we don't trigger a pipeline if there is no result trigger."""
        trigger = _prepare({'something': 'unrelated'})
        with self.assertLogs(level='INFO', logger=brew.LOGGER) as log:
            brew.process_result_message('gitlab', [trigger], {})
            self.assertIn('No trigger found to handle results message!',
                          log.output[-2])

    @mock.patch('triggers.brew_trigger.put_gitlab_queue')
    def test_pipeline_triggered(self, mock_trigger):
        """Check we trigger a pipeline with correct variables."""
        message = {'team_name': 'cool team', 'team_email': 'cool@team.test',
                   'cki_pipeline_id': 111, 'results': [],
                   'summarized_result': 'PASS'}
        trigger = {'result_pipe': 'True', 'cki_project': 'cki/project',
                   'cki_pipeline_branch': 'result-branch'}
        expected_trigger = {
            'status': 'success',
            'original_pipeline_id': 111,
            'team_email': 'cool@team.test',
            'team_name': 'cool team',
            'title': 'Results from cool team for #111',
            'cki_pipeline_type': 'result',
            'data': '{}{}'.format(
                '/Td6WFoAAATm1rRGAgAhARYAAAB0L+WjAQABW10AAABTtu6JouwuKAABGg',
                'LcLqV+H7bzfQEAAAAABFla'
            ),
            'result_pipe': 'True',
            'cki_project': 'cki/project',
            'cki_pipeline_branch': 'result-branch'}

        brew.process_result_message('gitlab', [trigger], message)
        mock_trigger.assert_called_with('gitlab', expected_trigger)
