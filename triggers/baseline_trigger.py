"""Pipeline trigger for baselines.

1. Get current top commit of the requested repo/branch.
2. Check if it was already tested.
3. Profit $$$.
"""
import copy
from datetime import datetime
import hashlib

from cki_lib import cki_pipeline
from cki_lib import config_tree
from cki_lib.logger import get_logger

from . import utils

LOGGER = get_logger(__name__)


def generate_hash(git_url, commit):
    """Generate a sha256 hash used as cki_pipeline_id.

    Args:
        git_url: URL of the git repo being tested.
        commit:  Git commit which is being tested.

    Returns:
        A sha256 digest of formatted string of arguments.
    """
    string = f'{git_url}@{commit}'
    return hashlib.sha256(string.encode('utf-8')).hexdigest()


def load_triggers(gitlab_instance, baselines_config, _):
    """Get ready all potential triggers."""
    triggers = []
    for key, value in baselines_config.items():
        for branch in value['.branches']:
            trigger = copy.deepcopy(value)
            project = cki_pipeline.pipeline_project(gitlab_instance, trigger)

            trigger['commit_hash'] = utils.get_commit_hash(
                trigger['git_url'], f'refs/heads/{branch}'
            )
            if trigger['commit_hash'] is None:
                continue

            trigger['discovery_time'] = f'{datetime.utcnow().isoformat()}Z'
            trigger['cki_pipeline_id'] = generate_hash(trigger['git_url'],
                                                       trigger['commit_hash'])
            trigger['branch'] = branch
            trigger['name'] = key
            trigger['cki_pipeline_type'] = 'baseline'
            trigger['require_manual_review'] = trigger.get(
                'require_manual_review', True
            )

            if not trigger.get('make_target'):
                trigger['make_target'] = 'targz-pkg'

            do_not_trigger = utils.was_tested(project,
                                              trigger['cki_pipeline_branch'],
                                              trigger['cki_pipeline_id'])

            if do_not_trigger:
                LOGGER.info('Pipeline for %s@%s already triggered.',
                            trigger['branch'],
                            trigger['commit_hash'])
            else:
                trigger['title'] = 'Baseline: {} {}:{}'.format(
                    trigger['name'],
                    trigger['branch'],
                    trigger['commit_hash'][0:12]
                )
                triggers.append(config_tree.clean_config(trigger))
    return triggers
