"""Common main() for all triggers."""
import argparse
import logging
import os

from cki_lib import cki_pipeline
from cki_lib import config_tree
from cki_lib import misc
from cki_lib.logger import FORMAT
import gitlab
import sentry_sdk
from yaml import Loader
from yaml import load

from . import TRIGGERS


def main():
    """Initialize common code and run a specific trigger."""
    if 'SENTRY_DSN' in os.environ:
        # the DSN is read from SENTRY_DSN env variable
        sentry_sdk.init(ca_certs=os.getenv('REQUESTS_CA_BUNDLE'))

    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    parser = argparse.ArgumentParser(description='Pipeline triggers')
    parser.add_argument(
        '-c', '--config',
        type=str,
        help=(
            'YAML configuration file to use. Will try to use `<name>.yaml` '
            'from the current working directory if not specified, where '
            '`<name>` is one of: {}'.format(list(TRIGGERS.keys()))
        )
    )
    parser.add_argument(
        '--private-token', default='GITLAB_PRIVATE_TOKEN',
        help='Name of the env variable that contains the GitLab private token'
        ' (default: GITLAB_PRIVATE_TOKEN)')
    parser.add_argument('trigger',
                        choices=TRIGGERS.keys(),
                        help=('Name of the pipeline trigger'))
    parser.add_argument(
        '--kickstart',
        action='store_true',
        help=('Kickstart the pipelines. Instead of checking for the previously'
              ' existing pipelines, submit the newest available one, without '
              'sending the emails. Note that this created pipeline might be '
              'invalid. Not all triggers support this option.')
    )
    parser.add_argument(
        "--variables",
        action=misc.StoreNameValuePair,
        nargs='+',
        help=(
            'Variables to be added/overriden in all pipelines triggered '
            'e.g. --variables skip_beaker=true --variables mail_to=nobody'
        ),
        default={},
    )
    args = parser.parse_args()

    gitlab_url = misc.get_env_var_or_raise('GITLAB_URL')
    private_token = misc.get_env_var_or_raise(args.private_token)
    configfile = args.config if args.config else '{}.yaml'.format(args.trigger)

    with open(configfile) as config_file:
        config = load(config_file, Loader=Loader)

    gitlab_instance = gitlab.Gitlab(gitlab_url,
                                    private_token=private_token,
                                    api_version=4)

    if args.trigger == 'brew':
        # This one should run all the time and trigger the pipeline itself
        TRIGGERS[args.trigger].poll_triggers(gitlab_instance,
                                             config)
    else:
        # configure a logging handler and level
        # the Brew trigger code already does that via
        # cki_lib.logger.get_logger, but the others don't...
        logging.basicConfig(format=FORMAT, level=logging.INFO)

        config = config_tree.process_config_tree(config)
        loader = TRIGGERS[args.trigger].load_triggers
        triggers = loader(gitlab_instance, config, args.kickstart)
        cki_pipeline.trigger_multiple(gitlab_instance, triggers,
                                      extra_variables=args.variables)


if __name__ == '__main__':
    main()
