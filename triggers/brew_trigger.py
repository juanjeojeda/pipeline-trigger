"""Trigger for Brew/Koji builds using UMB.

See https://pagure.io/fedora-ci/messages for details.
"""
import base64
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed
import copy
from datetime import datetime
import json
import logging
import lzma
import os
from queue import Queue
import re
import time

from cki_lib import cki_pipeline
from cki_lib import config_tree
from cki_lib import logger
from cki_lib import messagequeue
from pipeline_tools import brew_trigger
import proton
from proton import handlers
from proton import reactor

LOGGER = logger.get_logger(__name__)


def queue_watcher(queue):
    """Once a minute, log the queue length."""
    while True:
        LOGGER.info('queue_size=%i', queue.qsize())
        time.sleep(60)


def gitlab_trigger(gitlab_queue, gitlab_instance):
    """Process GitLab trigger requests."""
    while True:
        trigger_to_use = gitlab_queue.get()
        try:
            while os.path.exists('/run/pipeline-trigger/brew.suspend'):
                LOGGER.info('Triggering suspended, sleeping')
                time.sleep(60)
            cki_pipeline.trigger_multiple(gitlab_instance, [trigger_to_use])
            gitlab_queue.task_done()
        except Exception:  # pylint: disable=broad-except
            LOGGER.exception('Unable to trigger pipeline for %s',
                             trigger_to_use)


def put_gitlab_queue(gitlab_queue, trigger_to_use):
    """Put a GitLab trigger request in a queue (for easy mocking)."""
    gitlab_queue.put(trigger_to_use)


def process_queue(queue, gitlab_queue, triggers,
                  brew_config):
    """Pick the correct function to process each message in the queue."""
    while True:
        message, server_section = queue.get()
        if 'copr' in message:
            process_copr(gitlab_queue,
                         triggers,
                         message)
        elif 'cki_pipeline_id' in message:
            process_result_message(gitlab_queue,
                                   triggers,
                                   message)
        else:
            process_message(gitlab_queue,
                            brew_config,
                            server_section,
                            message)
        queue.task_done()


def sanity_check(properties):
    """Verify the message is what we want.

    Args:
        properties: A dictionary of message properties.

    Returns:
        False if the message is "bad", True if we should continue processing.
    """
    if 'cki.results' in properties.get('topic', ''):
        return True

    if properties.get('attribute') != 'state':
        return False

    # We are not interested in messages that don't mark a completed task. Also,
    # official koji builds like to use numbers.
    if properties.get('new') not in ['CLOSED', 1]:
        return False

    # We are only interested in completed builds. koji official messgaes are
    # special again.
    if properties.get('method') != 'build' and 'build_id' not in properties:
        return False

    return True


def process_copr(gitlab_queue, triggers, message):
    """Process a message from COPR."""
    # Exclude empty automated nightly builds
    if not message.get('pkg') or not message.get('version'):
        return

    nvr = '{}-{}'.format(message['pkg'], message['version'])
    if message.get('status') != 1:
        LOGGER.debug('COPR build for %s not successful', nvr)
        return

    LOGGER.info('COPR build for %s found', nvr)
    copr = '{}/{}'.format(message['owner'], message['copr'])

    trigger_to_use = None
    for trigger in triggers:
        if 'result_pipe' in trigger:
            continue  # This is not the trigger we are looking for

        if re.search(r'^{}-\d+[.-](\S+[.-])+{}'.format(trigger['package_name'],
                                                       trigger['rpm_release']),
                     nvr):
            if copr in trigger.get('.coprs', []):
                trigger_to_use = trigger.copy()
                break
    if not trigger_to_use:
        LOGGER.info('COPR: Pipeline for %s not configured', nvr)
        return

    architecture = message['chroot'].split('-')[-1]

    trigger_to_use['owner'] = message['user']
    trigger_to_use['nvr'] = nvr
    trigger_to_use['copr_build'] = str(message['build'])
    trigger_to_use['title'] = f'COPR: {nvr}: {architecture}'
    trigger_to_use['arch_override'] = architecture
    trigger_to_use['repo_name'] = copr
    trigger_to_use['cki_pipeline_type'] = 'copr'
    trigger_to_use['discovery_time'] = f'{datetime.utcnow().isoformat()}Z'

    trigger_to_use = config_tree.clean_config(trigger_to_use)
    put_gitlab_queue(gitlab_queue, trigger_to_use)


def process_result_message(gitlab_queue, triggers, message):
    """Process a UMB result message."""
    trigger_to_use = None
    for trigger in triggers:
        if str(trigger.get('result_pipe')) == 'True':
            trigger_to_use = trigger.copy()

    if not trigger_to_use:
        LOGGER.info('No trigger found to handle results message!')
        LOGGER.info('%s', message)
        return

    team = message.get('team_name', 'UNKNOWN')
    team_email = message.get('team_email', '')
    original_pipeline_id = message['cki_pipeline_id']
    if message.get('summarized_result', '').lower() in ['pass', 'success']:
        status = 'success'
    else:
        status = 'fail'

    trigger_to_use.update({
        'status': status,
        'data': base64.b64encode(
            lzma.compress(json.dumps(message['results']).encode())
        ).decode(),
        'cki_pipeline_type': 'result',
        'title': f'Results from {team} for #{original_pipeline_id}',
        'original_pipeline_id': original_pipeline_id,
        'team_name': team,
        'team_email': team_email
    })

    trigger_to_use = config_tree.clean_config(trigger_to_use)
    put_gitlab_queue(gitlab_queue, trigger_to_use)


def process_message(gitlab_queue, brew_config, server_section,
                    message):
    """Process a message from Koji or Brew."""
    if 'task_id' in message:
        task_id = message['task_id']  # Koji official builds
    else:
        task_id = message['id']  # Koji scratch builds and Brew
    if not task_id:   # Some koji builds don't have related tasks
        return
    LOGGER.info('%s: A build completed!', task_id)

    if not message.get('request'):
        LOGGER.info('%s: Task doesn\'t have a build request, ignoring',
                    task_id)
        return

    trigger_to_use = brew_trigger.get_brew_trigger_variables(
        task_id, brew_config, server_section)

    if trigger_to_use:
        put_gitlab_queue(gitlab_queue, trigger_to_use)


class AMQPReceiver(handlers.MessagingHandler):
    """Receive messages from Brew."""

    def __init__(self, cert_path, urls, topics, server_section, queue):
        # pylint: disable=too-many-arguments
        """Initialize all the values we need."""
        super().__init__()

        self.cert_path = cert_path
        self.urls = urls
        self.topics = topics
        self.server_section = server_section
        self.queue = queue

    def on_start(self, event):
        """Connect to topics."""
        ssl = proton.SSLDomain(proton.SSLDomain.MODE_CLIENT)
        ssl.set_credentials(self.cert_path, self.cert_path, None)
        ssl.set_trusted_ca_db('/etc/ssl/certs/ca-bundle.crt')
        ssl.set_peer_authentication(proton.SSLDomain.VERIFY_PEER)
        conn = event.container.connect(urls=self.urls, ssl_domain=ssl)

        for topic in self.topics:
            event.container.create_receiver(conn, source=topic)

    def on_message(self, event):
        """Handle a single message.

        Check if we care about the message and add it to the queue if we do.
        """
        message = json.loads(event.message.body)
        properties = event.message.properties
        if 'info' in message:
            message = message['info']

        if sanity_check(properties):
            self.queue.put((message, self.server_section))

    def on_link_opened(self, event):
        # pylint: disable=no-self-use
        """Log successful link info."""
        LOGGER.info('Link opened to %s at address %s',
                    event.connection.hostname,
                    event.link.source.address)

    def on_link_error(self, event):
        """Log link errors."""
        LOGGER.error('Link error: %s: %s',
                     event.link.remote_condition.name,
                     event.link.remote_condition.description)
        LOGGER.info('Closing connection to %s', event.connection.hostname)
        event.connection.close()
        raise Exception('Link error occured!')

    def on_connection_error(self, event):
        """Log connection errors."""
        handlers.EndpointStateHandler.print_error(event.connection,
                                                  'connection')
        event.connection.close()
        raise Exception('Connection error occured!')


class AMQP091Receiver:
    """Receive messages from an AMQP 0.9.1 server."""

    def __init__(self, config, server_section, queue):
        """Initialize a message queue, but do not connect yet."""
        self.connection = messagequeue.MessageQueue(
            host=config['.host'],
            port=config['.port'],
            cafile=config['.cafile'],
            certfile=config['.certfile'],
            connection_params={'virtual_host': config['.virtual_host']},
            dlx_retry=False
        )
        self.exchange = config['.exchange']
        self.routing_keys = config['.routing_keys']
        self.server_section = server_section
        self.queue = queue

    def callback(self, topic, message):
        """Handle an individual message."""
        if 'copr' in topic:
            self.queue.put((message, None))
        elif sanity_check(message):
            if 'info' in message:  # Only scratch builds
                message = message['info']
            self.queue.put((message, self.server_section))

    def receive_messages(self):
        """Endlessly receive messages."""
        self.connection.consume_messages(
            self.exchange, self.routing_keys,
            self.callback, queue_name=os.environ.get('BREW_QUEUE'))


def get_triggers_from_config(brew_config):
    """Transform config file to individual triggers."""
    triggers = []
    for key, value in \
            config_tree.process_config_tree(brew_config).items():
        trigger = copy.deepcopy(value)
        trigger['name'] = key
        triggers.append(trigger)
    return triggers


def poll_triggers(gitlab_instance, brew_config):
    """Create the actual triggers and start the receiver."""
    triggers = get_triggers_from_config(brew_config)

    queue = Queue()
    gitlab_queue = Queue()
    executor = ThreadPoolExecutor(max_workers=10)
    futures = []

    if brew_config.get('.amqp091'):
        amqp_091 = AMQP091Receiver(brew_config['.amqp091'], '.amqp091', queue)
        logging.getLogger('pika').setLevel(logging.WARNING)
        futures.append(executor.submit(amqp_091.receive_messages))

    if brew_config.get('.amqp'):
        logging.getLogger('proton').setLevel(logging.WARNING)
        logging.getLogger('proton.reactor').setLevel(logging.WARNING)
        logging.getLogger('proton.handlers').setLevel(logging.WARNING)
        amqp_receiver = reactor.Container(AMQPReceiver(
            brew_config['.amqp']['.cert_path'],
            brew_config['.amqp']['.receiver_urls'],
            brew_config['.amqp']['.message_topics'],
            '.amqp',
            queue
        ))
        futures.append(executor.submit(amqp_receiver.run))

    for _ in range(4):
        futures.append(executor.submit(process_queue,
                                       queue,
                                       gitlab_queue,
                                       triggers,
                                       brew_config))

    futures.append(executor.submit(queue_watcher, queue))
    futures.append(executor.submit(gitlab_trigger,
                                   gitlab_queue,
                                   gitlab_instance))

    for finished in as_completed(futures):
        LOGGER.error('Thread exited unexpectedly!')
        try:
            result = finished.result()
        # pylint: disable=broad-except
        except Exception as exc:
            LOGGER.error('Got exception: %s', exc)
        else:
            LOGGER.error('Got result: %s', result)
        # pylint: disable=protected-access
        os._exit(1)  # How to actually kill all threads... sigh
