How to run things
-----------------

Execute `python3 -m triggers <trigger_name> <options>`. All the triggers except
the brew one are supposed to finish. The brew trigger is supposed to start and
run permanently, as it's listening to UMB messages live.


Kickstarting the pipeline
-------------------------
The Patchwork pipelines need to know the ID of the last tested patch or event in
order to grab patches newer than that. If this is the first time you are running
the pipeline it's obviously not possible. What the trigger should automatically
grab is unclear in this case -- should it be all existing patches? Last week?
Only the last patch? Different people may expect different outcome, hence we
didn't implement any of these.

Instead we implemented a `--kickstart` option which grabs the last patch series
(Patchwork2) or the last patch (Patchwork1) and submits a dummy pipeline which
results shouldn't be sent anywhere. After this step all the newer patches
should be picked up for testing.


Writing new triggers - tl;dr
----------------------------

* By default, a `<trigger-name>.yaml` file is expected to contain the trigger
  configuration. For testing, you can create any file you want and pass it over
  with the `--config <filename>` option.
* Create a `<trigger-name>_trigger.py` file in the `triggers/` directory. This
  file contains the code needed for transformation of the configuration and any
  external data into the variables passed to the actual pipeline trigger.
* Add a new trigger key to the mapping in `triggers/__init__.py`.
* Write tests for your code !!!


Some more details, hints and suggestions
----------------------------------------

* If possible, use the functions in `triggers/utils.py` instead of writing new
  ones for the same functionality. It makes it easier to test and maintain.
* The entry point of the `<trigger-name>_trigger.py` should be a
  `load_triggers` function that takes 3 arguments, which are passed by the
  `main()` function:
  * `gitlab_instance` -- an `gitlab.Gitlab` object
  * `config` -- parsed YAML configuration
  * `kickstart` -- Instead of checking the last tested pipelines before
                   submitting new ones, create a new dummy pipeline instead. The
                   trigger can decide whether to implement this functionality or
                   not, as it may not make sense to be used there.
* `load_triggers()` should return a list of dictionaries, each dictionary fully
  describing the pipeline to trigger. The mandatory parameters are:
  * `cki_project`: The project to trigger the pipelines in, in the
                   `username/repo` format. Should be read from the
                   configuration.
  * `cki_pipeline_branch`: Branch of the `cki_project` which should be used for
                           reference commit creation and pipeline triggering.
                           Should also be read from the configuration.
  * `title`: Title that should be used to identify the commit. Should be created
             based on what's tested.
  * `cki_pipeline_type`: A string describing the type of the test, eg.
    `baseline` or `patchwork`.


Full list of supported configuration options
--------------------------------------------

Some of these are needed by our pipeline or reporting systems so if you want to
run the triggers yourself you may not need them (OTOH feel free to add any
variables your pipeline needs to the config file). Others are added by the
trigger itself. _Please check out the example of the config file if you wish to
use brew trigger since that requires additional configuration for receivers._

* `cki_project` - the GitLab project that holds the pipeline to be triggered
* `cki_pipeline_project` - the GitLab subproject that holds the pipeline to be
  triggered. The complete project is built from the user/project given in the
  `GITLAB_PARENT_PROJECT` environment variable. If this variable is used,
  `cki_project` should not be specified. At the moment, this is only supported
  by the baseline trigger.
* `cki_pipeline_branch` - the branch in the `cki_project` that should receive
  a commit to start a pipeline run. This variable is not a reliable indicator
  for anything and should not be used inside the pipeline!
* `cki_pipeline_type` - the pipeline type; currently supported:

  * baseline
  * brew
  * patchwork
  * patchwork-v1

* `tree_name` - the base name of the tree GitLab YAML file used to configure
  the pipeline. The complete include path is built from the tree name and the
  `PIPELINE_DEFINITION_URL` environment variable. If this variable is provided,
  the triggers can create the `.gitlab-ci.yml` file for non-existing branches
  in the pipeline repository.
* `config_target` - determines how the kernel configs should be built
  (for example _tinyconfig_)
* `cover_letter` - Patchwork URL to the cover letter of the patch series being
  tested; empty string if the cover letter does not exist
* `last_patch_id` - the last Patchwork patch ID from specific series; used by
  Patchwork1 pipeline trigger to know where to begin looking for new patches
  later (Patchwork2 uses the event ID to find new patches)
* `make_target` - how to build the kernel in the pipeline; not currently used
  (example: _targz-pkg_)
* `name` - the pipeline name (example: `upstream-stable`)
* `patchwork_project` - the project linkname for the kernel tree in Patchwork
* `patchwork_session_cookie` - the session cookie to use when talking to a
  Patchwork server which requires authentication
* `patchwork_url` - URL to the Patchwork instance to use
* `skip_XXX`, where XXX can be any pipeline step

  * If `True`, skip the step in the pipeline
  * If `False` or not present, run the step in the pipeline

* `title` - the title of the commit made into the pipeline repository to
  identify the testing
* `mail_add_maintainers_to` - denotes how test maintainers should be addressed
  on the report email (examples: _cc_, _bcc_ or _to_)
* `mail_bcc` - email recipients to add to the `BCC:` email field
* `mail_from` - the email address to use in the report email's `From:` field
* `mail_to` - email recipients to add to the `To:` email field
* `manual_review_mail_to` - email recipients for manual review emails
  (also see `require_manual_review`)
* `message_id` - the message id of the last patch in the patch series; reports
  should use this ID with the `In-Reply-To` email header
* `require_manual_review` - determine how to handle a test that failed:

  * If `True`, send a _Waiting for review_ email to recipients and require the
    manual step in the GitLab pipeline to be clicked before a report will be
    sent.

  * If `False`, send reports immediately to the relevant kernel mailing list
    without waiting for additional review.

* `send_report_to_upstream` - used internally by the pipeline-triggers project
  to determine if the kernel mailing list and patch author should be included
  in the `To:` field of the report email.

  * If `True`, send a report to the mailing list and patch authors when the test
    is complete.
  * If `False`, do not send a report. Useful for onboarding of new kernels.

* `send_report_on_success`

  * If `True`, send reports for all pipelines, regardless of success/failure.
  * If `False`, only send reports for failed pipelines.

* `subject` - the subject line to use when sending the report
* `baserepo` - the URL to the git repo to be cloned
* `branch` - The branch to check out after the repo is cloned
* `ref` - the refspec to check out from the kernel repository, such as
  _master_, a tag, or a specific SHA hash to a particular commit
* `min_commit_age` - don't trigger unless the commits from git are at least X
  minutes old.
* `report_types` - comma-separated list of strings describing how the pipeline
  result should be reported. Currenly supported: `email`, `osci`,
  `umb-result-email`. For upstream pipelines, only `email` is applicable. In the
  future, Patchwork checks and other types may be supported. Defaults to `email`
  if not specified.
* `publish_elsewhere` - `True` if the built kernel should be moved to a
  different place instead of staying in GitLab. This option only supports
  internal kernels, don't use it for upstream trees.
* `mail_to_task_owner` - `True` if the email report should be sent to the
  Brew/Koji/COPR task owner. The email is derived as `<username>@redhat.com` so
  it's currently only applicable to internal developers. Defaults to `False`.
* `result_pipe` - `'True'` if the configuration marks UMB result pipeline.
  Ignored elsewhere.
* `send_ready_for_test_pre` - `True` if a "ready for test" UMB message should be
  sent before CKI testing starts (after the "setup" stage completed). Defaults
  to `False`.
* `send_ready_for_test_post` - `True` if a "ready for test" UMB message should
  be sent after CKI testing completed. Defaults to `False`. Useful for e.g.
  performance tests.
* `arch_override` - A string describing which architectures should be tested.
  If missing, all architectures defined in the associated pipeline tree will be
  tested. Format is a string with space separated values, such as
  `"x86_64 ppc64le s390x"`. Only a subset of architectures defined in the
  pipeline tree can be chosen, additional values will be ignored. Variable is
  taken from Brew build overrides and extended to work with other pipelines.
* `artifacts_visibility` - `public` if the pipeline artifacts (generated bins, logs, etc.)
  should be published on a public facing storage. Defaults to `private`.
* `ARTIFACT_URL_${architecture}` - Used by the test stage on certain pipelines
  to retrieve build artifacts from a different pipeline. Because of this, it is also
  used by the `kcidb` module on `cki-tools` in order to be able to recreate the `build_id`
  for the test jobs.
* `ARTIFACT_URL_${architecture}_debug` - The same as `ARTIFACT_URL_${architecture}`, but
  present on debug kernels.
* `disttag_override` - Override the default `%dist` RPM macro. Only applicable for RPM builds.
* `download_separate_headers` - The kernel headers are not included in the
  Brew/Koji task itself, try to download them via `cki.cki_tools.get_kernel_headers`.
* `fedora_macro_disable` - `true` to set `%fedora` RPM macro to `nil`. Only needed when
  building non-Fedora RPMs on Fedora.
* `zstream_latest` - Set to `true` if this baseline pipeline belongs to the
  newest zstream. This allows the bot to select the newest baseline pipelines
  for RHEL version where no ystream is available.


Environment variables
---------------------

* `IS_PRODUCTION` - if false, variables will be cleaned for retriggering.
  Emails are removed, the pipeline is marked as a retrigger, latest ystream
  that passed testing is used, and last patch tracking, Patchwork variables,
  and Beaker are disabled.
* `GITLAB_URL`,`GITLAB_PRIVATE_TOKEN` - info for GitLab
  instance to trigger pipeline on
* `GITLAB_PARENT_PROJECT` - user/project prepended to `cki_pipeline_project`
* `PATCHWORK_USER`,`PATCHWORK_PASSWORD` - credentials for Patchwork v1


Format and dummy examples of currently supported config files
-------------------------------------------------------------

Baseline:
```
name:
  git_url: url
  .branches:
    - branch-to-test
    - another-branch-to-test
  cki_project: CKI-project/cki-pipeline
  cki_pipeline_branch: cki-branch
  any-other-vars: here

name2:
  git_url: url2
  .branches:
    - branch-to-test
    - another-branch-to-test
  cki_pipeline_project: cki-pipeline
  cki_pipeline_branch: cki-branch-2
  any-other-vars: here
```

Brew/Koji/COPR:
```
.amqp:
  .message_topics:
    - list-of-topics-here
    - 'topic://VirtualTopic.eng.brew.task.closed.>'
  .receiver_urls:
    - 'amqps://receiver:port'
    - 'amqps://receiver2:port'
  .cert_path: filepath
  server_url: http://koji-example.org/kojihub
  web_url: http://koji-example.org/koji
  top_url: http://pkgs.koji-example.org/
.zmq:
  .message_topics:
    - 'org.fedoraproject.prod.copr.build.end'
    - 'org.fedoraproject.prod.buildsys.build.state.change'
  .receiver_urls:
    - 'tcp://hub:port'
  server_url: http://koji-example.org/kojihub
  web_url: http://koji-example.org/koji
  top_url: http://pkgs.koji-example.org/
.default:
  any-global-variables: here
name-fedora:
  .extends: .zmq
  .test_scratch: 'False'
  .coprs:
    - username/copr
    - another/one
  rpm_release: 'fc30'
  package_name: kernel
  cki_project: CKI-project/brew-pipeline
  cki_pipeline_branch: fedora
  any-pipeline-specific-variables: here
name-fedora-rt:
  .extends: .zmq
  .test_scratch: 'True'
  .owners:
    - only-trigger-for-this-user
    - and-this-user
  rpm_release: 'fc30'
  package_name: kernel-rt
  cki_project: CKI-project/brew-pipeline
  cki_pipeline_branch: fedora
```

Patchwork (both v1 and v2, although they need to be kept in separate files):
```
name:
  git_url: url
  branch: branch
  cki_project: CKI-project/cki-pipeline
  cki_pipeline_branch: cki-branch
  patchwork:
    url: http://patchwork.example.org
    project: example-project
  any-other-vars: here
```


UMB result message format
-------------------------

For cases of additional testing done by different teams, the results should be
sent via UMB to `/topic/VirtualTopic.eng.cki.results`. The following result
format is expected:

```
{'cki_pipeline_id': 123,
 'results': [
   {'test_arch': <arch>,
    'test_description': <desc>,
    'test_log_url': [list, of, urls],
    'test_name': <name>,
    'test_result': <result>,
    'test_waived': <waived_status>,
    'is_debug': <True or False>},
   {'test_arch': <arch>,
    'test_description': <desc>,
    'test_log_url': [list, of, urls],
    'test_name': <name>,
    'test_result': <result>,
    'test_waived': <waived_status>,
    'is_debug': <True or False>},
   ....
 ],
 'summarized_result': <result>,
 'team_email': <email>,
 'team_name': <name>
}
```

Message properties only need to include the `topic` attribute.

* `cki_pipeline_id`: Integer, ID of the original pipeline (sent in the
  triggering message)
* `results`: List of per-task results. A separate entry for each executed test
  is required.
* `test_arch`: String representing the architecture that was tested. This
  attribute may change in the future to something more comprehensive to handle
  e.g. specific CPU families.
* `is_debug`: Bool value marking the debug status of the tested kernel. Defaults
  to `False` if not present.
* `test_log_url`: List of strings with URLs to detailed logs.
* `test_name`: String containing a short test name.
* `test_description`: String containing test description, meant for humans to
  understand what the test is doing. Used in reports as a single test can be
  executed in multiple variants which makes the test name itself not as useful.
* `test_result`: String representing test results, can be `PASS` or `FAIL`.
* `test_waived`: String representing whether the test results are reliable or
  not. `True` if the test is waived (and results may not be reliable), `False`
  otherwise. Useful for adding new tests.
* `summarized_result`: Combined result of all executed tests. String, can be
  `PASS` or `FAIL`. If only waived tests failed and everything else passed, the
  attribute should be `PASS`.
* `team_email`: String, email contact for the team responsible for testing. This
  email is CCed on reports so the developers can ask about details directly by
  replying to the message.
* `team_name`: String, human-readable name of the team doing the testing.

Override/add variables in all pipelines triggered
-------------------------------------------------

If you need override or add a variable into all pipelines, you can use the
`--variables` parameter. For instance, if you want to avoid submitting jobs to
beaker, write down `--variables skip_beaker=true`, it will add into all
pipelines triggered the variable `skip_beaker = true`.

Using inheritance in the pipeline configuration
-----------------------------------------------

For the triggers that support multiple trigger configurations, basic
inheritance and defaults modeled after the
[default](https://docs.gitlab.com/ce/ci/yaml/#global-defaults) and
[extends](https://docs.gitlab.com/ce/ci/yaml/#extends) keywords in
the [GitLab CI/CD job descriptions](https://docs.gitlab.com/ce/ci/yaml) can be
used.

- trigger configurations with a name starting with a dot (`.`) are not
  processed, but can be used for inheritance
- the `.default` trigger configuration will be inherited by all trigger
  configurations
- the configuration option `.extends` followed by a name or list of names
  specifies trigger configuration(s) to inherit from
- configuration options with a name starting with a dot (`.`) are used to
  configure the trigger, but are NOT passed to the pipeline code as trigger
  variables

Partial example:

```yaml
.zmq:
  .cert_path: '/etc/cki/umb/cki-bot-prod.pem'
  server_url: 'https://server.org/'
.default:
  mail_from: CKI Project <cki-project@redhat.com>
  send_report_to_upstream: 'False'
.send_upstream:
  send_report_to_upstream: 'True'
fedora-30:
  .extends: .zmq
  package_name: kernel
  rpm_release: 'fc31'
fedora-31:
  .extends: [fedora-30, .send_upstream]
  rpm_release: 'fc31'
  .coprs:
    - username
```

Suspending the brew trigger
---------------------------

The brew trigger can be suspended by logging into the pod with something like

```shell
kubectl exec -it brew-trigger /bin/bash
```

or

```shell
oc rsh dc/brew-trigger /bin/bash
```

and placing an empty file at `/run/pipeline-trigger/brew.suspend`.
